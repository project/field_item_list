<?php

namespace Drupal\field_item_list\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Render\Element;

/**
 * Plugin implementation of the 'entity reference label' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_label_list",
 *   label = @Translation("Label list"),
 *   description = @Translation("Display the label of the referenced entities as list."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceLabelListFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'list_type' => 'ol',
    ] + parent::defaultSettings();
  }

  public static function getListTypeOptions() {
    return [
      'ol' => t('Ordered list'),
      'ul' => t('Unordered list'),
      'comma' => t('Comma separated'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements = parent::settingsForm($form, $form_state);
    $elements['list_type'] = [
      '#title' => t('List type'),
      '#type' => 'select',
      '#options' => static::getListTypeOptions(),
      '#default_value' => $this->getSetting('list_type'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $list_type_options = static::getListTypeOptions();
    $summary[] = $this->t('List type: @list_type', ['@list_type' => $list_type_options[$this->getSetting('list_type')]]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $element_items = $this->viewElements($items, $langcode);

    $item_list = [
      '#theme' => 'item_list',
      '#items' => $element_items,
    ];
  
    $list_type = $this->getSetting('list_type');

    if (in_array($list_type, ['ul', 'ol'])) {
      $item_list['#list_type'] = $list_type;
    }
    else if ($list_type == 'comma') {
      $item_list['#context'] = ['list_style' => 'comma-list'];
    }

    $elements = parent::view($items, $langcode);
    $elements['#theme'] = 'field_item_list';
    $elements['#items'] = $item_list;
    return $elements;
  }
}
